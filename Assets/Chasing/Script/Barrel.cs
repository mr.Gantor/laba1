﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Barrel : MonoBehaviour
{
    [SerializeField] UnityEvent playerEnter;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
            playerEnter?.Invoke();
    }
}
