﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PlayerState : MonoBehaviour
{
    int barrel;
    int damag;

    [SerializeField] Text barrelCount;
    [SerializeField] GameObject[] lifeIcon;

    [SerializeField] UnityEvent playerDead;
    [SerializeField] UnityEvent playerWin;

    private void Start()
    {
        barrel = 0;
        damag = 0;
    }

    public void takeBarrel() 
    {
        barrel += 1;
        barrelCount.text = barrel + "/5";

        if (barrel == 5)
        {
            playerWin?.Invoke();
            Time.timeScale = 0;
        }
    }

    public void takeDamag() 
    {
        if (damag < 3)
        {
            damag += 1;
            lifeIcon[damag - 1].SetActive(false);

            if (damag == 3)
                playerDead?.Invoke();
        }

    }


}
