﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieController : MonoBehaviour
{
    [SerializeField] Transform playerPosition;  // позиция игрока в пространстве
    [SerializeField] Transform[] potrolPosition; // координаты точек по которым ведется патрулирование
    NavMeshAgent zombie; // ссылка на объект агента
    Animator animator;

    float _speedWalk = 1; // скорость агента во время ходьбы
    [SerializeField]float _speedRun = 5f; // скорость агента во аремя бега или приследования
    bool playerFound; // переменная отображающая виден ли игрок сейчас нашему объекту

    IEnumerator coroutineWaiting;

    // Start is called before the first frame update
    void Start()
    {
        // кешируем компоненты для удобства
        zombie = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetFloat("Speed", zombie.speed); // передача аниматору текущую скорость агента

        if (playerFound) 
        {
            if (coroutineWaiting != null) 
            {
                StopCoroutine(coroutineWaiting); // если игрок обнаружен выводим агента из режима ожидания
                coroutineWaiting = null; // обнуляем сопрограмму
            } 

            zombie.destination = playerPosition.position; // задаем новое место назначения с координатами игрока
    
            if (zombie.remainingDistance < 1f)
            {
                animator.Play("Z_Attack"); // если агент подошел к игроку ближе чем на 1 юнит то переходим к анимации атаки
            }
        }
        else
        if (coroutineWaiting == null && zombie.remainingDistance < 0.1f) // если агент не обнаружил игрока и прише в точку птрулирования то ожидает случайное время 
        {
            coroutineWaiting = Waiting();
            StartCoroutine(coroutineWaiting);
        }
    }

    public void GoToPlayer() // если игрок обнаружен агент начинает к нему бежать
    {
        playerFound = true;
        zombie.speed = _speedRun;
        zombie.destination = playerPosition.position;
    }

    public void GoPotrol() 
    {
        playerFound = false;
        zombie.speed = _speedWalk; // скорость ходьбы 
        zombie.destination = potrolPosition[Random.Range(0, potrolPosition.Length)].position; // выбирается случайная точка
        coroutineWaiting = null; // обнуление сопрограммы
    }

    IEnumerator Waiting() 
    {
        yield return new WaitForSeconds(Random.Range(3, 6)); // ожидает от 3 до 6 секунд и выбирает новую точку патрулирования
        GoPotrol();
    }

}

