﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScene : MonoBehaviour
{
    public void changeScene(string name) 
    {
        Time.timeScale = 1;
        SceneManager.LoadSceneAsync(name);
    } 
}
